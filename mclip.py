#! python 3
# mclip.py - A multi-clipboard program.

import pyperclip, sys

# holds the key phrases and their text
TEXT = {
    'agree': """Yes, I agree. That sounds fine to me""",
    'busy': """Sorry, can we do this later this week or next week?""",
    'upsell': """Would you consider making this a monthly donation?"""
}
# handle command-line arguments - the zeroth item in sys.argv is the program, the first is the first command line arg
if len(sys.argv) < 2:
    print('Usage: python mclip.py [keyphrase] - copy phrase text')
    sys.exit()

keyphrase = sys.argv[1] # first command line arg is the keyphrase

# if the keyphrase requested in available, copy the corresponding text to the clipboard.
if keyphrase in TEXT:
    pyperclip.copy(TEXT[keyphrase]) #pyperclip.copy will copy its given argument to the clipboard
    print(f'Text for {keyphrase} copied to keyboard.')
else:
    print(f'There is no text for {keyphrase}.')

