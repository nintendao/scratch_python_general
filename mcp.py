#! python3
# mcb.pyw - Saves and loads pieces of text to the clipboard.
# Usage: py.exe mcb.pyw save <keyword> - Saves clipboard to keyword.
#        py.exe mcb.pyw <keyword> - Loads keyword to clipboard.
#        py.exe mcb.pyw list - Loads all keywords to clipboard.
import shelve, pyperclip, sys
''' Whenever the user wants to save a new piece of clipboard text, you’ll save it to a shelf file. 
Then, when the user wants to paste the text back to their clipboard, you’ll open the shelf file and load it back 
into your program.
'''
mcbShelf = shelve.open('mcb')
#TODO: Save clipboard content
#TODO: List keywords and load content
mcbShelf.close()
