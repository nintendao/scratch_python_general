grid = [['.', '.', '.', '.', '.', '.'],
        ['.', 'O', 'O', '.', '.', '.'],
        ['O', 'O', 'O', 'O', '.', '.'],
        ['O', 'O', 'O', 'O', 'O', '.'],
        ['.', 'O', 'O', 'O', 'O', 'O'],
        ['O', 'O', 'O', 'O', 'O', '.'],
        ['O', 'O', 'O', 'O', '.', '.'],
        ['.', 'O', 'O', '.', '.', '.'],
        ['.', '.', '.', '.', '.', '.']]

numOfRows = len(grid)
numOfCols = len(grid[0])

for x in range(numOfRows):
    for y in range(numOfCols):
        print(grid[x][y], end='') # at the end of a loop for a x coordinate, do not append an end character (default \n)
    print() # new line


