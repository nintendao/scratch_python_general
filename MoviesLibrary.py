#! python3
# This program stores my list of movies to watch.
# save movie_name: saves movie_name to the list of movies
# check movie_name: searches movie_name from the list of movies
# get movie_name: goes to torrent site of movie for downloading
import webbrowser, sys, requests, logging, re, shelve


logging.basicConfig(level = logging.DEBUG, format = '%(asctime)s - %(levelname)s - %(message)s')
logging.basicConfig(level = logging.INFO, format = '%(asctime)s - %(levelname)s - %(message)s')
# logging.disable(logging.DEBUG)


movieShelf = shelve.open('moviesLibrary_Shelf') # open binary dictionary (key-value) file
if 'toWatch' not in movieShelf:
    movieShelf['toWatch'] = []
# movieShelf.close()

thePirateBayString = "" # will be sys.argv

imdb = "https://www.imdb.com"
# imdbSearch = f'/find?q={imdbString}'
thePirateBayUrl = "https://www.pirate-bay.net/search?q=gloria+bell?q=gloria+bell"
thePirateBaySearch = ""

argumentList = sys.argv[2:]
# TODO: accept a movie to be added to the list of movies to watch
    # TODO: accept a comma-separated list of movies to be added to the list of movies to watch
if sys.argv[1].lower() == 'save':
    movieShelfList = list(movieShelf['toWatch'])  # open the 'toWatch' values, as a list
    movieName = []
    commaRegex = re.compile(r',$')
    start = 0
    for word in argumentList:
        currentIndex = argumentList.index(word) # current index of the argument
        # the word ends in a comma or the current index is the last argument in the list
        if commaRegex.search(word) is not None or (currentIndex + 1) == len(argumentList):
            end = argumentList.index(word) + 1
            # the movie name is from the first word to the last word ending in a comma
            movieName.append(' '.join(argumentList[start:end]))
            start = end
            # append the movie to the existing list of movies to watch
            movieShelfList.append(movieName)
            movieName = []
    # overwrite the existing movie list with the new movie list
    movieShelf['toWatch'] = movieShelfList
    movieShelf.close()
    # TODO: remove the comma from the end of movie names

if sys.argv[1].lower() == 'get':

    # TODO: character translation - identify remaining special characters
    '''
    , => %2
    / => %2F
    & => %26

    '''
    movieSearch = '+'.join(argumentList)
    logging.info(f'the search string is {movieSearch}')
    imdbUrl = imdb + f'/find?q={movieSearch}'
    print(f'taking you to {imdbUrl}:')

    webbrowser.open(imdbUrl)
'''
spaceRegex = re.compile(r'\s')
imdbSearchString = "Gloria Bell 2018" # will be sys.argv
searchFormat = []






# webbrowser.open(imdbUrl)
# webbrowser.open(thePirateBayUrl)
# TODO: open a link to the website in imdb, given a movie title


'''

# TODO: web scrape the result page, click on the top result, take you to that page

print(movieShelfList)