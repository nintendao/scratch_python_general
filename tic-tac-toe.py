# this data structure represents the tic-tac-toe board. All empty keys represent a clear board.
theBoard = {
    'top-L':'',
    'top-M':'',
    'top-R':'',
    'mid-L':'',
    'mid-M':'',
    'mid-R':'',
    'bot-L':'',
    'bot-M':'',
    'bot-R':''
}

# print the tic-tac-toe board
def printBoard(board):
    print('|' + board['top-L'] +'|' + board['top-M'] + '|' + board['top-R'] + '|')
    print('-+-+-')
    print('|' + board['mid-L'] +'|' + board['mid-M'] + '|' + board['mid-R'] + '|')
    print('-+-+-')
    print('|' + board['bot-L'] +'|' + board['bot-M'] + '|' + board['bot-R'] + '|')
    return

# the game
print('Enter any key to begin')
input()
turn = 'X'

# for every turn, update the board with the given move
for moves in theBoard:
    print('It is ' + turn + '\'s' + ' turn.')
    print('Make your move:')
    move = input() # one of the keys
    theBoard[move] = turn
    if turn == 'X':
        turn = '0'
    else:
        turn = 'X'
    printBoard(theBoard)
