#! python3
# mcb.pyw - Saves and loads pieces of text to the clipboard
# Usage: py.exe mcb.pyw save <keyword> - Saves clipboard to keyword.
#        py.exe mcb.pyw <keyword> - Loads keyword value to clipboard.
#        py.exe mcb.pyw list - Loads all keywords to clipboard.
#        py.exe mcb.pyw delete <keyword> - Deletes the specified keyword from the binary file

import shelve, pyperclip, sys

mcbShelf = shelve.open('mcb') # create/open a binary shelf file 'mcb'

# use case: save the clipboard to a key
# py.exe mcb.pyw save <keyword>
with mcbShelf:
    if len(sys.argv) == 3 and sys.argv[1].lower() == 'save':
        print(f'{pyperclip.paste()} is about to be saved to the key \'{sys.argv[2]}\'')
        runSave = 0
        while runSave == 0:
            print('Enter y to proceed')
            proceed = input()
            if proceed.lower() == 'y':
                runSave = 1
                mcbShelf[sys.argv[2]] = pyperclip.paste()  # the last argument is the key
            elif proceed.lower() == 'n':
                runSave = 0
                print('Program cancelled')
            else:
                runSave = 0

    # use case: delete the specified key from the binary file
    # py.exe mcb.pyw delete <keyword>
    elif len(sys.argv) == 3 and sys.argv[1].lower() == 'delete':
        print(f'The key \'{sys.argv[2]}\' is about to be deleted. Text is:\n{mcbShelf[sys.argv[2]]}')
        runDelete = 0
        while runDelete == 0:
            print('Enter y to proceed')
            proceed = input()
            if proceed.lower() == 'y':
                runDelete = 1
                del mcbShelf[sys.argv[2]] # the last argument is the key to be deleted
                print('Deleted')
            elif proceed.lower() == 'n':
                print('NOT Deleted')
                runDelete = 1
            else:
                runDelete = 0

    elif len(sys.argv) == 2:
        # use case: list the keys
        # py.exe mcb.pyw list
        if sys.argv[1].lower() == 'list':
            pyperclip.copy(str(list(mcbShelf.keys())))
            print(f'Copied to clipboard:\n{str(list(mcbShelf.keys()))}')
        # use case: load the requested key value to the clipboard
        # py.exe mcb.pyw <keyword>
        elif sys.argv[1] in mcbShelf:
            pyperclip.copy(mcbShelf[sys.argv[1]])
            print(f'Copied to clipboard:\n{mcbShelf[sys.argv[1]]}')
        else:
            print(f'Key {sys.argv[1]} not found.')



# TODO: Allow the program to persist, and not close until requested by the user


