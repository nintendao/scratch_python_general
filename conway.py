import random, time, copy

WIDTH = 60
HEIGHT = 20

#create the initial cells
nextCells = []

for x in range(WIDTH):
    column = []
    for y in range (HEIGHT):
        if random.randint(0,1) == 0:
            column.append('#')
        else:
            column.append(' ')
    nextCells.append(column)

while True:
    # print the cells
    # currentCells = copy.deepcopy(nextCells)
    print('\n\n\n\n\n')
    currentCells = copy.deepcopy(nextCells)
    for y in range(HEIGHT):
        for x in range(WIDTH):
            print(currentCells[x][y], end='')
        print()

# determine the state of the next set of cells
    for x in range(WIDTH):
        for y in range(HEIGHT):
            # assign the coordinates of neighbouring cells
            left = (x - 1) % WIDTH
            right = (x + 1) % WIDTH
            above = (y - 1) % HEIGHT
            below = (y + 1) % HEIGHT

            # determine the number of living neighbour cells
            aliveNeighbours = 0
            if currentCells[left][y] == '#':
                aliveNeighbours += 1
            if currentCells[left][above] == '#':
                aliveNeighbours +=1
            if currentCells[x][above] == '#':
                aliveNeighbours +=1
            if currentCells[right][above] == '#':
                aliveNeighbours +=1
            if currentCells[right][y] == '#':
                aliveNeighbours +=1
            if currentCells[right][below] == '#':
                aliveNeighbours +=1
            if currentCells[x][below] == '#':
                aliveNeighbours +=1
            if currentCells[left][below] == '#':
                aliveNeighbours +=1

            # if a living square has two or three living neighbours, it continues to live on the next step
            if currentCells[x][y] == '#' and (aliveNeighbours == 2 or aliveNeighbours == 3):
                nextCells[x][y] = '#'
            #if a dead square has exactly three living neighbours, it comes alive on the next step
            elif currentCells[x][y] == ' ' and aliveNeighbours == 3:
                nextCells[x][y] = '#'
            # every other square dies or remains dead on the next step
            else:
                nextCells[x][y] = ' '

    time.sleep(1)
