from typing import List

spam: List[str] = ['apples', 'bananas', 'tofu', 'cats']

"""
Write a function that takes a list value as an argument and returns a string with all the items separated by a 
comma and a space, with and inserted before the last item. For example, passing the previous spam list to the 
function would return 'apples, bananas, tofu, and cats'. But your function should be able to work with any 
list value passed to it. Be sure to test the case where an empty list [] is passed to your function.
"""

def CommaCode(list_arg):
    all_items = ""
    counter = 0
    for i in list_arg:
        # if the loop is iterating through the last list item, don't insert the comma and space
        if counter + 1 < len(list_arg):
            all_items += i + ", "
        else:
            all_items += i
        counter += 1
    print(all_items)
    return

CommaCode(spam)