def collatz(number):

    if number % 2 == 0:
        return (number//2)
    else:
        return (3*number + 1)

def yayCollatz():
    itsCollatz = False
    runCount = 0

    print('Type a number:')
    collatzNumber = int(input())

    while itsCollatz == False:
        currentEval = collatz(collatzNumber)
        runCount = runCount+1
        print('We\'re currently at ' + str(currentEval))
        if currentEval != 1:
            collatzNumber = currentEval
        else:
            itsCollatz = True
    if itsCollatz:
        return print('Finished at ' + str(runCount) + ' executions.')

yayCollatz()
