import requests, logging, os
from pathlib import Path

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
# logging.disable(logging.DEBUG)

print(Path.cwd())
# os.chdir('D:\\code')
print(Path.cwd())
print(os.getcwd())

res = requests.get('https://umart.com.au')
try:
    res.raise_for_status()
except Exception as exc:
    print(f' There was a problem: {exc}')

playFile = open(Path('Users/nintendao/Downloads/romeoAndJuliet.txt'))
for chunk in res.iter_content(100000):
    playFile.write(chunk)
    logging.debug(f'size of chunk is {len(chunk)}')
playFile.close()

