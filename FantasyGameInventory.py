import time

inventoryOfNintendao = {'rope': 1, 'torch': 6, 'gold coin': 42, 'dagger': 1, 'arrow': 12}
dragonLoot = ['gold coin', 'dagger', 'gold coin', 'gold coin', 'ruby']

# function to read an inventory and display its contents: each item's name and how many of each item
def displayInventory(inventory):
    # total number of items counter
    itemTotal = 0
    print('Inventory: ')
    #loop through the inventory and print out the item and its amount
    for item, itemCount in inventory.items():
        print(str(itemCount) + ' ' + item)
        # add the amount of each item to the total
        itemTotal += itemCount

    #print out the total
    return print('Total number of items: ' + str(itemTotal) + '\n')

"""
Imagine that a vanquished dragon’s loot is represented as a list of strings like this:
dragonLoot = ['gold coin', 'dagger', 'gold coin', 'gold coin', 'ruby']

Write a function named addToInventory(inventory, addedItems), where the inventory parameter is a dictionary 
representing the player’s inventory (like in the previous project) and the addedItems parameter is a list like 
dragonLoot. The addToInventory() function should return a dictionary that represents the updated inventory. Note 
that the addedItems list can contain multiples of the same item.
"""
# function to take a list of item drops add its contents to the player's inventory
def addToInventory(inventory, addedItems):
    print('Picking up:')
    time.sleep(0.3)
    for drop in addedItems:
        # create a new item if it doesn't yet exist in the player's inventory
        inventory.setdefault(drop, 0)
        inventory[drop] += 1

    return print('inventory updated with your loot! \n')

# show inventory, add loot, show inventory again
displayInventory(inventoryOfNintendao)
addToInventory(inventoryOfNintendao, dragonLoot)
displayInventory(inventoryOfNintendao)

