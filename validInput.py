# first loop accepts numbers only for age
while True:
    print('Please enter your age:')
    age = input()
    if age.isdecimal():
        break
    print('Numbers only, nerd.')

# second loop accepts letters and numbers only for the password
while True:
    print('Please enter a password:')
    password = input()
    if password.isalnum():
        break
    print('letters and numbers only, nerd.')

