picnicItems = {'sandwiches': 4, 'apples': 12, 'cups': 4, 'cookies': 8000}

def printPicnic(itemsDict, lWidth, rWidth):
    print('PICNIC ITEMS'.center(lWidth + rWidth, '-'))
    for item, count in itemsDict.items():
        print(item.ljust(lWidth,'.') + str(count).rjust(rWidth))
    return

printPicnic(picnicItems, 12, 5)
printPicnic(picnicItems, 20, 6)