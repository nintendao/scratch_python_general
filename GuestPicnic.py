allGuests = {'Alice': {'apples': 5, 'pretzels': 12},
             'Bob': {'ham sandwiches': 3, 'apples': 2},
             'Carol': {'cups': 3, 'apple pies': 1}}

# function to find the number of items brought by a guest
def totalBrought(guests, item):
    numBrought = 0
    # multiple assignment trick in a for loop to assign the key and value to separate variables
    for key, value in guests.items():
        numBrought += value.get(item, 0)
    return numBrought

print('Apples: ' + str(totalBrought(allGuests, 'apples')))
print('Pretzels: ' + str(totalBrought(allGuests, 'pretzels')))
print('Ham Sandwiches: ' + str(totalBrought(allGuests, 'ham sandwiches')))
print('Cups: ' + str(totalBrought(allGuests, 'cups')))
print('Apple pies: ' + str(totalBrought(allGuests, 'apple pies')))
print('Donuts: ' + str(totalBrought(allGuests, 'donuts')))
