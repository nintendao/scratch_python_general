#! python3
# bulletPointAdder.py - Adds Wikipedia bullet points to the start of each line of text on the clipboard.

import pyperclip, sys

# grab the text from the clipboard to TEXT
TEXT = pyperclip.paste()

# TODO: Separate lines and add starts

#Copy the new text to the clipboard
pyperclip.copy(TEXT)