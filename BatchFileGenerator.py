#! python3
# BatchFileGenerator.py - creates a running batch file (.bat) for running your Python Program in Windows
from pathlib import Path
import os
# TODO: turn into function
scriptName = 'MoviesLibrary'
batchPath = str(Path('D:/','code','bat'))
batchWrite = open(f'{batchPath}/{scriptName}.bat','w')
scriptPath = str(Path('D:/','code','py_TestProgram'))

# TODO: Dynamic path for script files
# TODO: Dynamic path for bat files

with batchWrite as bfw:
    '''
    The %* forwards any command line arguments entered after the batch filename to the Python script.
    The @ sign at the start of each command prevents it from being displayed in the terminal window.  
    '''
    bfw.write(f'@python {scriptPath}/{scriptName}.py %*\n')
    bfw.write('@pause') # adds 'Press any key to continue...' at the end of the script's execution

