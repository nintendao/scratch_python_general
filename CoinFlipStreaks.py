import random

"""
For this exercise, we’ll try doing an experiment. If you flip a coin 100 times and write down an “H” for each heads and
“T” for each tails, you’ll create a list that looks like “T T T T H H H H T T.” If you ask a human to make up 100 
random coin flips, you’ll probably end up with alternating head-tail results like “H T H T H H T H T T,” which looks 
random (to humans), but isn’t mathematically random. A human will almost never write down a streak of six heads or six
tails in a row, even though it is highly likely to happen in truly random coin flips. Humans are predictably bad at 
being random.

Write a program to find out how often a streak of six heads or a streak of six tails comes up in a randomly generated 
list of heads and tails. Your program breaks up the experiment into two parts: the first part generates a list of 
randomly selected 'heads' and 'tails' values, and the second part checks if there is a streak in it. Put all of this 
code in a loop that repeats the experiment 10,000 times so we can find out what percentage of the coin flips contains 
a streak of six heads or tails in a row.
"""

def CoinFlipStreaks(numOfRuns = 10000):
    sixtreakCounter = 0 # streak counter
    # heads and tails counters
    headsCounter = 0
    tailsCounter = 0

    # generate list of heads and tails values: 10000 experiments
    coinFlipList = []
    for x in range(numOfRuns):
        if random.randint(0,1) == 0:
            coinFlipList.append('H')
        else:
            coinFlipList.append('T')

    # iterate through the list of runs
    for flip in coinFlipList:
        # if it is heads, increase the counter for heads and reset the counter of tails
        if flip == 'H':
            headsCounter += 1
            tailsCounter = 0
        # vice versa for tails
        else:
            tailsCounter += 1
            headsCounter = 0
        # check if there is a 'six streak' in it
        if headsCounter == 6 or tailsCounter == 6:
            # record the streak then reset the streak counters
            sixtreakCounter += 1
            headsCounter = 0
            tailsCounter = 0

    streakPercentage = (sixtreakCounter / numOfRuns) * 100

    print (str(streakPercentage) + '% of flips were streaks')

    return streakPercentage

CoinFlipStreaks()