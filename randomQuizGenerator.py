#! python3
# randomQuizGenerator.py - Creates quizzes with questions and answers in random order, along with the answer key
from pathlib import Path
import random

# The quiz data. Keys are states and values are their capitals.
capitals = {
    'Alabama': 'Montgomery', 'Alaska': 'Juneau', 'Arizona': 'Phoenix', 'Arkansas': 'Little Rock',
    'California': 'Sacramento', 'Colorado': 'Denver', 'Connecticut': 'Hartford',
    'Delaware': 'Dover', 'Florida': 'Tallahassee', 'Georgia': 'Atlanta', 'Hawaii': 'Honolulu',
    'Idaho': 'Boise', 'Illinois': 'Springfield', 'Indiana': 'Indianapolis', 'Iowa': 'Des Moines',
    'Kansas': 'Topeka', 'Kentucky': 'Frankfort', 'Louisiana': 'Baton Rouge', 'Maine': 'Augusta',
    'Maryland': 'Annapolis', 'Massachusetts': 'Boston', 'Michigan': 'Lansing',
    'Minnesota': 'Saint Paul', 'Mississippi': 'Jackson', 'Missouri': 'Jefferson City',
    'Montana': 'Helena', 'Nebraska': 'Lincoln', 'Nevada': 'Carson City', 'New Hampshire': 'Concord',
    'New Jersey': 'Trenton', 'New Mexico': 'Santa Fe', 'New York': 'Albany',
    'North Carolina': 'Raleigh', 'North Dakota': 'Bismarck', 'Ohio': 'Columbus',
    'Oklahoma': 'Oklahoma City', 'Oregon': 'Salem', 'Pennsylvania': 'Harrisburg',
    'Rhode Island': 'Providence', 'South Carolina': 'Columbia', 'South Dakota': 'Pierre',
    'Tennessee': 'Nashville', 'Texas': 'Austin', 'Utah': 'Salt Lake City', 'Vermont': 'Montpelier',
    'Virginia': 'Richmond', 'Washington': 'Olympia', 'West Virginia': 'Charleston',
    'Wisconsin': 'Madison', 'Wyoming': 'Cheyenne'
}

filePath = str(Path('D:/','Documents','Quizzes'))

# Generate 35 quiz files
for quizNum in range(35):
    # TODO: Create the quiz and answer key files
    quizFile = open(f'{filePath}/capitalsquiz_{quizNum + 1}.txt','w')
    answerKeyFile = open(f'{filePath}/capitalsquiz_answers_{quizNum + 1}.txt','w')
    # TODO: Write out the header for the quiz
    quizFile.write('Name:\n\n\Date:\n\nPeriod:\n\n')
    quizFile.write((' ' * 20) + f'State Capitals Quiz (Form {quizNum + 1})')
    quizFile.write('\n\n')

    # TODO: Shuffle the order of the states
    states = list(capitals.keys())
    random.shuffle(states) # shuffles the list of states in place (returns None)

    # TODO: Loop through all 50 states, making a question for each
    for questionNum in range(50):
        # Get right and wrong answers
        correctAnswer = capitals[states[questionNum]] # evaluates to the value of the state
        wrongAnswers = list(capitals.values())
        del wrongAnswers[wrongAnswers.index(correctAnswer)] # wrong answers are everything minus the correct answer
        wrongAnswers = random.sample(wrongAnswers, 3) # only need three wrong answers
        answerOptions = wrongAnswers + [correctAnswer]
        random.shuffle(answerOptions)

        # TODO: Write the question and answer options to the quiz file
        quizFile.write(f'{questionNum + 1}. What is the capital of {states[questionNum]}?\n')
        for i in range(4):
            quizFile.write(f"   {'ABCD'[i]}. { answerOptions[i]}\n") # this expression treats the string 'ABCD' as an array
        quizFile.write('\n')
        # TODO: Write the answer key to a file
        answerKeyFile.write(
                f"{questionNum + 1}. {'ABCD'[answerOptions.index(correctAnswer)]}\n"
                )
    quizFile.close()
    answerKeyFile.close()


