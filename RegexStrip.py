import re
#! python3
# function to remove a given argument from a given string.
# if no remove argument is given, the function will strip the start and end of the string of any whitespaces

def stripRegex(stringArg, stripChar = '\s'):
    if stripChar == '\s': # default argument removes the start and end space characters
        regexCharStart = re.compile(r'^(%s)+' % stripChar).sub('', stringArg)
        regexCharEnd = re.compile(r'(%s)+$' % stripChar).sub('', regexCharStart)
        return regexCharEnd
    else:
        regexChar = re.compile(r'%s' % stripChar).sub('', stringArg) # replace regex matches with '' i.e. remove them
        return regexChar

text = '        Science says that space is real. What does this program think about this?       '
print(stripRegex(text))
print(stripRegex(text, 'space'))