# Read the command line arguments from sys.argv.
# Read the clipboard contents.
# Call the webbrowser.open() function to open the web browser.


import webbrowser, sys, logging, re, pyperclip

logging.basicConfig(level=logging.DEBUG, format=' %(asctime)s - %(levelname)s - %(message)s')
logging.basicConfig(level=logging.INFO, format=' %(asctime)s - %(levelname)s - %(message)s')
logging.disable(logging.DEBUG)

# function to clean up the address given in sys.argv
def address_url_formatted(address):
    # assert '  ' not in address, provided address has more than one space character!'
    address_formatted = ''
    regex_separator = re.compile(r'[\s+,]')
    for i in range(len(address)):
        # replace the space character with plus only if the character after the space is not also a space
        if not regex_separator.search(address[i]) is None:
            if regex_separator.search(address[i + 1]) is None:
                address_formatted += '+'
        else:
            address_formatted += address[i]
        logging.debug(f'address_formatted is {address_formatted}')

    logging.info(f'function returned {address_formatted}')
    return address_formatted


# TODO: handle arguments passed to the script via os execution (sys.argv)


#  currently userString is acting as a pseudo sys.argv
userString = ['program','870,Valencia','St','','',', ','','','','','','','San Francisco,','CA 94110']
userStringTwo = ['program','870,Valencia','St','','',', ','','','','','','','San Francisco,','CA 94110']

if len(userString) > 1:
    userAddress = ' '.join(userString[1:])
else:
    userAddress = pyperclip.paste()
logging.info(f'userAddress is {userAddress}')

location = address_url_formatted(userAddress)
googleMaps = 'https://www.google.com/maps/place'

mapsUrl = f'{googleMaps}/{location}'
logging.info(f'url looks like {mapsUrl}')
webbrowser.open(mapsUrl)