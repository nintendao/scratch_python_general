import pprint # pprint() and pformat() functions  will “pretty print” a dictionary’s values.
message = 'It was a bright cold day in April, and the clocks were striking thirteen.'
count = {}

for character in message:
    # if the character does not exist, add it as a key to the dictionary with starting value (count) of 0
    count.setdefault(character, 0)
    count[character] = count[character] + 1

pprint.pformat()