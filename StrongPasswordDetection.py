#! python3
import re, pyperclip
# This function ensures that the password is at least eight characters long, contains both uppercase and lowercase
# characters, and has at least one digit.

def strongPasswordDetection(passwordArg):

    def searchPasswordArg(regex):     # function to search the password against a given regex, returns 1 if detected.
        for char in range(len(passwordArg)):
            searchPassword = regex.search(passwordArg[char])
            if not searchPassword is None:
                return 1

        return 0

    regexDigit = re.compile(r'\d') # regex for detecting numbers
    regexUpperChar = re.compile(r'[A-Z]') # regex for detecting uppercases
    regexLowerChar = re.compile(r'[a-z]') # regex for detecting lowercases
    erray = [] # array of password errors

    if not searchPasswordArg(regexDigit) == 1:
        erray.append('Must include a number')

    if not searchPasswordArg(regexUpperChar) == 1:
        erray.append('Must include a uppercase')

    if not searchPasswordArg(regexLowerChar) == 1:
        erray.append('Must include a lowercase')

    if len(passwordArg) < 8:
        erray.append('Password is too short')

    if len(erray) > 0: # empty error array means the password is valid
        print ('Password is not valid:'.center(50,'-'))
        for err in erray:
            print(f'{err}'.ljust(15))
        return False
    else:
        print('Password is valid!')
        return True

while True:
    print('Please input your password: ')
    passwordInput = input()
    if strongPasswordDetection(passwordInput) == True:
        break



