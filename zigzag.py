import time, sys

def zigZag(indentStep, maxIndent, char):
    indentIncreasing = True
    indent = 0
    while True:
        try:
            print(' ' * indent, end='')
            print(char * 10)
            time.sleep(.1)
            if indent == maxIndent:
                indentIncreasing = False
            if not indentIncreasing:
                indent = indent-indentStep
            else:
                indent = indent + indentStep
            if indent == 0:
                indentIncreasing = True
        except KeyboardInterrupt:
            sys.exit()

zigZag(2, 20, '+')