#!python3
# reads in text files and lets the user add their own text anywhere the word ADJECTIVE, NOUN, ADVERB, or VERB appears
# in the text file.
import re
from pathlib import Path

text = 'The ADJECTIVE panda walked to the NOUN and then VERB. A nearby NOUN was unaffected by these events.'
regexMadLibs = re.compile(r'ADJECTIVE|NOUN|ADVERB|VERB', re.IGNORECASE)
searchText = regexMadLibs.findall(text)

print(f'text is:\n{text}\n')
for word in searchText:
    print(f'Please replace the {word}:\n')
    replacer = input()
    text = regexMadLibs.sub(replacer, text, 1)
    print(f'{text}\n')

# TODO: Save text to a file
# TODO: Allow the text to be provided by the user