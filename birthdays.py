birthdays = {'alice': 'dec 12', 'bob': 'mar 3', 'charlie': 'april 6'}

while True:
    print('Whose birthday do you want to check?')
    name = input()

    if name == "":
        break
    if name in birthdays:
        print("the birthday for " + name + " is on " + birthdays[name])
    else:
        print("there is no birthday for that person. What is the day of their birthday?")
        birthdayDate = input()
        birthdays[name] = birthdayDate
        print("birthdays updated.")

