#! python3
# TextFileSniffer.py - opens all .txt files in a folder and searches for any line that matches a user-supplied regex.
# Results are printed to the screen.
import os, re
from pathlib import Path

def TextFileSniffer(regex):
    text_files_folder = Path('D:/','code','txt')
    text_files_regex = re.compile(r'.*\.txt$')
    text_files_list = []
    matched_lines_list = []
    in_folder = os.listdir(text_files_folder)
    regex = re.compile(rf'{regex}')

    # Search through the filepath
    for file in range(len(in_folder)):
        searchFile = text_files_regex.search(in_folder[file]) # filter all non-text files
        if not searchFile == None:
            text_files_list.append(searchFile.group())
    # print(in_folder)
    # print(text_files_list)

    for text_file in range(len(text_files_list)):
        readFile = open(text_files_folder / text_files_list[text_file]).readlines() # Separate all strings by new lines
        # pprint.pprint(readFile)
        for line in range(len(readFile)):
            matchedLine = regex.search(readFile[line]) # Search each string for the given regex
            if not matchedLine == None: # Return the full string of a matched regex to a matched list
                matched_lines_list.append(readFile[line])

    # Print the strings, as well as the number of matched strings (length of matched list)
    print(f'Number of lines containing {regex}: {len(matched_lines_list)}')
    print('\n')
    for line in matched_lines_list:
        print(line)

    return matched_lines_list

print('Enter the words you wish to find (regular expressions allowed):')
userInput = input()

TextFileSniffer(userInput)