import re, pyperclip
#! python3

regexDate = re.compile(r'(\d\d)/(\d\d)/(\d\d\d\d)')

regexDateTest = regexDate.findall('01/02/0303  05/06/0707   01/02/2020  29/02/2020  29/02/2021')
validDates = []
for date in range(len(regexDateTest)):
    validDate = 0
    # TODO: validation for 30 and 31st days of all months
    if (int(regexDateTest[date][0]) != 0 or not int(regexDateTest[date][0]) > 31 # check valid day
    ) and (int(regexDateTest[date][1]) != 0 or not int(regexDateTest[date][1]) > 12 # check valid month
    ) and (not int(regexDateTest[date][2]) < 1000 and not int(regexDateTest[date][2]) > 2999): # check valid year
        if not int(regexDateTest[date][0]) == 29 and not int(regexDateTest[date][1]) == 2: # not feb 29
            validDate = r'/'.join(regexDateTest[date])
            validDates.append(validDate)
        else:
            if (int(regexDateTest[date][2]) % 4 == 0 #feb 29 can only occur on a leap year: https://www.mathsisfun.com/leap-years.html
            ) and (int(regexDateTest[date][2]) % 100 != 0 or int(regexDateTest[date][2]) % 400 == 0):
                validDate = r'/'.join(regexDateTest[date])
                validDates.append(validDate)
            # else:
                # do nothing

print (validDates)

